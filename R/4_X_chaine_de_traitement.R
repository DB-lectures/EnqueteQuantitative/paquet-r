#'@example
#'  .DossierCsv=system.file("extdata/collecte20230207",package='EQ2023data')
#'  .FichierSuiviCollecte=system.file("extdata/echantilloninternet_suivi.ods",package='EQ2023data')  
#'Donnees<-ChaineDeTraitement()
ChaineDeTraitement<-function(
  .DossierCsv=system.file("extdata/collecte20230207",package='EQ2023data'),
  .FichierSuiviCollecte=system.file("extdata/echantilloninternet_suivi.ods",package='EQ2023data'))  {
  ImporteLesDonnees(.DossierCsv)$DonneesBrutes|>
    Nettoyage()|>
    Ajoute.Info.Auxiliaire(.FichierSuiviCollecte)|>
    Correction()|>
    Enleve_hors_champ()|>
    CalculdesPoids()}



