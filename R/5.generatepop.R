Lycees<-data.frame()|>
  dplyr::mutate()


#'@examples
#'N<-1000
RandomPop<-function(N=1000){
  data.frame(
      SEXE=factor(c("Homme", "Femme", "Autre"),levels=c("Homme", "Femme", "Autre"))|>
        sample(size=N,prob=c(.45,.45,.1),rep=T))|>
    dplyr::mutate(
      ANAISS=sample(1995:2003,size=N,rep=T),
      NIVEAU_ETUDE=paste0("BAC +",c(1:5,"6 ou plus"))|>
        (function(x){factor(x,levels=x)})()|>
        sample(size=N,rep=T),
      CURSUS=rep("",N),
      DEP_ETUDE=rep("",N),
      COM_ETUDE=c("",N),
      DEP_RES=rep("",N),
      COM_RES=c("",N),
      LOGEMENT=c("Chez les parents",
                 "En résidence universitaire",
                 "Seul hors résidence universitaire",
                 "En colocation",
                 "En couple sans enfant",
                 "En couple avec enfant(s)",
                 "Autre situation")|>
        (function(x){factor(x,levels=x)})()|>
        sample(size=N,rep=T),
      LOGEMENT_PREC=ifelse(LOGEMENT!="Autre situation",NA,""),
      TRAVAIL=c("Jamais ou occasionnellement",
      "Moins de 10 heures par semaine 2",
      "De 10 à 19 heures par semaine 3",
      "De 20 à 29 heures par semaine 4",
      "30 heures ou plus par semaine 5")|>
        (function(x){factor(x,levels=x)})()|>
        sample(size=N,rep=T),
    SPORTIF=c("1. Plus de 3 fois par semaine 1",
    "1 ou 2 fois par semaine 2",
    "Moins de 4 fois par mois 3",
    "Rarement 4",
    "Jamais")|>
      (function(x){factor(x,levels=x)})()|>
      sample(size=N,rep=T),
  JOP_SPORT=c("Oui",
              "Non",
              "Sans opinion",
              stringi::stri_rand_strings(5,5))|>
    (function(x){factor(x,levels=x)})()|>
    sample(size=N,rep=T))
}


TYPES_SPORT=
  data.frame(TYPE_SPORT=
               c("Football",
                 "Tennis",
                 "Danse",
                 "Équitation",
                 "Natation",
                 "Course à pied",
                 "Musculation",
                 "Basketball",
                 "Cyclisme",
                 "Rugby",
                 "Escalade",
                 "Golf",
                 stringi::stri_rand_strings(5,5)))


LIEUX_SPORTS= data.frame(
  LIEUX_SPORTS=c(
                 "En club avec une licence",
                 "En club sans licence",
                 "A l’université",
                 "Dans une salle de sport",
                 "En autonomie",
                 stringi::stri_rand_strings(5,5)))

  
JOP_EPREUVES=data.frame(
  JOP_EPREUVES=c("Je ne sais pas",
  "Cyclisme",
  "Golf",
  "Sports équestres",
  "Voile",
  "Pentathlon moderne",
  "Marathon",
  stringi::stri_rand_strings(5,5)))  
  
  
JOP_PARTICIP=data.frame(
  JOP_PARTICIP=c("Regarder les Jeux Olympiques et Paralympiques à la télévision",
                 "Assister à des évènements sur place (y compris dans les fan zones)",
                 "Être bénévole",
                 stringi::stri_rand_strings(5,5)))  






FREINS=  data.frame(
  c(  "Manque de temps",
  "Manque d’argent",
    "Manque d’infrastructures à proximité ou inaccessible",
    "Problème de santé ou handicap",
    "Ne souhaite pas (re)faire du sport ou en faire davantage",
    stringi::stri_rand_strings(5,5)))



Enqueteurs<-
  
Plan<-function(pop){}

