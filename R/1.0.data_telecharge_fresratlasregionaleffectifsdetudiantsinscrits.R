#' @description
#' Telecharge le fichier csv depuis
#' https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-atlas_regional-effectifs-d-etudiants-inscrits/table/?disjunctive.rgp_formations_ou_etablissements&sort=-rentree&refine.annee_universitaire=2020-21&refine.niveau_geographique=Commune&refine.dep_id=D078
#' lien csv:
#' https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-atlas_regional-effectifs-d-etudiants-inscrits/download/?format=csv&disjunctive.rgp_formations_ou_etablissements=true&refine.annee_universitaire=2020-21&refine.niveau_geographique=Commune&refine.dep_id=D078&timezone=Europe/Berlin&lang=en&use_labels_for_header=true&csv_separator=%3B
#' @param fichier destination
#' @examples 
#' destfile=
#' file.path(Mydirectories::gitlab.directory(),"Lectures/2022-2023 _ UVSQ Enquêtes Quantitatives.R/Paquet R/inst/extdata/fr-esr-statistiques-sur-les-effectifs-d-etudiants-inscrits-par-etablissementCOM.csv")
#' destfile|>
#' telecharger_fresratlas_regionaleffectifsdetudiantsinscritsCOM() 
telecharger_fresratlas_regionaleffectifsdetudiantsinscritsCOM<-
  function(destfile=tempfile()){
    options(timeout = 300)
    utils::download.file("https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-atlas_regional-effectifs-d-etudiants-inscrits/download/?format=csv&disjunctive.rgp_formations_ou_etablissements=true&refine.annee_universitaire=2020-21&refine.niveau_geographique=Commune&refine.dep_id=D078&timezone=Europe/Berlin&lang=en&use_labels_for_header=true&csv_separator=%3B",
                         destfile=destfile)
    destfile}


#' @examples 
#' destfile=
#' file.path(Mydirectories::gitlab.directory(),"Lectures/2022-2023 _ UVSQ Enquêtes Quantitatives.R/Paquet R/inst/extdata/fr-esr-statistiques-sur-les-effectifs-d-etudiants-inscrits-par-etablissementDEP.csv")
#' destfile|>
#' telecharger_fresratlas_regionaleffectifsdetudiantsinscritsDEP() 
telecharger_fresratlas_regionaleffectifsdetudiantsinscritsDEP<-
  function(destfile=tempfile()){
    options(timeout = 300)
    utils::download.file("https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-atlas_regional-effectifs-d-etudiants-inscrits/download/?format=csv&disjunctive.rgp_formations_ou_etablissements=true&refine.annee_universitaire=2020-21&refine.niveau_geographique=D%C3%A9partement&refine.geo_nom=Yvelines&timezone=Europe/Berlin&lang=en&use_labels_for_header=true&csv_separator=%3B",
                         destfile=destfile)
    destfile}


#' @description
#' lit le fichier csv fr-esr-statistiques-sur-les-effectifs-d-etudiants-inscrits-par-etablissementCOM.csv
#'@param fichier destination
#' @examples 
#' destfile=
#' file.path(Mydirectories::gitlab.directory(),"Lectures/2022-2023 _ UVSQ Enquêtes Quantitatives.R/Paquet R/inst/extdata/fr-esr-atlas_regional-effectifs-d-etudiants-inscrits.csv")
#' destfile|>
#' telecharger_fresratlas_regionaleffectifsdetudiantsinscritsCOM()
#' destfile|>
#' lire_fresrstatistiquessurleseffectifsdetudiantsinscritsparetablissementCOM()->
#' fresratlas_regionaleffectifsdetudiantsinscritsCOM
#' save(fresratlas_regionaleffectifsdetudiantsinscritsCOM,file=
#' file.path(Mydirectories::gitlab.directory(),"Lectures/2022-2023 _ UVSQ Enquêtes Quantitatives.R/Paquet R/data/fresratlas_regionaleffectifsdetudiantsinscritsCOM.rda"))
lire_fresrstatistiquessurleseffectifsdetudiantsinscritsparetablissementCOM<-function(destfile){
  destfile|>
    read.csv(sep = ";")}

#' @description
#' lit le fichier csv fr-esr-statistiques-sur-les-effectifs-d-etudiants-inscrits-par-etablissement.csv
#'@param fichier destination
#' @examples 
#' destfile=
#' file.path(Mydirectories::gitlab.directory(),"Lectures/2022-2023 _ UVSQ Enquêtes Quantitatives.R/Paquet R/inst/extdata/fr-esr-atlas_regional-effectifs-d-etudiants-inscrits.csv")
#' destfile|>
#' telecharger_fresratlas_regionaleffectifsdetudiantsinscritsDEP()
#' destfile|>
#' lire_fresrstatistiquessurleseffectifsdetudiantsinscritsparetablissementDEP()->
#' fresratlas_regionaleffectifsdetudiantsinscritsDEP
#' save(fresratlas_regionaleffectifsdetudiantsinscritsDEP,file=
#' file.path(Mydirectories::gitlab.directory(),"Lectures/2022-2023 _ UVSQ Enquêtes Quantitatives.R/Paquet R/data/fresratlas_regionaleffectifsdetudiantsinscritsDEP.rda"))

lire_fresrstatistiquessurleseffectifsdetudiantsinscritsparetablissementDEP<-function(destfile){
  destfile|>
    read.csv(sep = ";")}


#' @description
#' Telecharge le fichier csv depuis
#' https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-statistiques-sur-les-effectifs-d-etudiants-inscrits-par-etablissement/table/?sort=-annee_universitaire
#' lien csv:
#' https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-statistiques-sur-les-effectifs-d-etudiants-inscrits-par-etablissement/download/?format=csv&timezone=Europe/Berlin&lang=en&use_labels_for_header=true&csv_separator=%3B
#' @param fichier destination
#' @examples 
#' destfile=
#' file.path(Mydirectories::gitlab.directory(),"Lectures/2022-2023 _ UVSQ Enquêtes Quantitatives.R/Paquet R/inst/extdata/fr-esr-statistiques-sur-les-effectifs-d-etudiants-inscrits-par-etablissement.csv")
#' destfile|>
#' telecharger_fresrstatistiquessurleseffectifsdetudiantsinscritsparetablissement() 
telecharger_fresrstatistiquessurleseffectifsdetudiantsinscritsparetablissement<-
  function(destfile=tempfile()){
    options(timeout = 300)
    utils::download.file("https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-statistiques-sur-les-effectifs-d-etudiants-inscrits-par-etablissement/download/?format=csv&timezone=Europe/Berlin&lang=en&use_labels_for_header=true&csv_separator=%3B",
                         destfile=destfile)
    destfile}

#' @description
#' lit le fichier csv fr-esr-statistiques-sur-les-effectifs-d-etudiants-inscrits-par-etablissement.csv
#'@param fichier destination
#' @examples 
#' destfile=
#' file.path(Mydirectories::gitlab.directory(),"Lectures/2022-2023 _ UVSQ Enquêtes Quantitatives.R/Paquet R/inst/extdata/fr-esr-statistiques-sur-les-effectifs-d-etudiants-inscrits-par-etablissement.csv")
#' destfile|>
#' telecharger_fresrstatistiquessurleseffectifsdetudiantsinscritsparetablissement()|>
#' destfile|>
#' lire_fresrstatistiquessurleseffectifsdetudiantsinscritsparetablissement()->
#' effectifsdetudiantsinscritsparetablissement
#' save(effectifsdetudiantsinscritsparetablissement,file=
#' file.path(Mydirectories::gitlab.directory(),"Lectures/2022-2023 _ UVSQ Enquêtes Quantitatives.R/Paquet R/data/effectifsdetudiantsinscritsparetablissement.rda"))
lire_fresrstatistiquessurleseffectifsdetudiantsinscritsparetablissement<-function(destfile){
  destfile|>
    read.csv(sep = ";")|>
    dplyr::filter(Année.universitaire=="2021-22",
                  etablissement_id_departement=="D078"
    )}



#' @description
#' Telecharge le fichier csv depuis
#' https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-atlas_regional-effectifs-d-etudiants-inscrits/export/?disjunctive.rgp_formations_ou_etablissements&sort=-rentree#' Telecharge
#'@param fichier destination
#' @examples 
#' destfile=
#' file.path(Mydirectories::gitlab.directory(),"Lectures/2022-2023 _ UVSQ Enquêtes Quantitatives.R/Paquet R/inst/extdata/fr-esr-atlas_regional-effectifs-d-etudiants-inscrits.csv")
#' destfile|>
#' telecharger_fresratlasregionaleffectifsdetudiantsinscrits() 
telecharger_fresratlasregionaleffectifsdetudiantsinscrits<-
  function(destfile=tempfile()){
  options(timeout = 300)
utils::download.file("https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-atlas_regional-effectifs-d-etudiants-inscrits/download/?format=csv&timezone=Europe/Berlin&lang=en&use_labels_for_header=true&csv_separator=%3B",
                     destfile=destfile)
destfile}


#' @description
#' Telecharge le fichier csv depuis
#' https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-atlas_regional-effectifs-d-etudiants-inscrits/export/?disjunctive.rgp_formations_ou_etablissements&sort=-rentree#' Telecharge
#'@param fichier destination
#' @examples 
#' destfile=
#' file.path(Mydirectories::gitlab.directory(),"Lectures/2022-2023 _ UVSQ Enquêtes Quantitatives.R/Paquet R/inst/estdata/fr-esr-atlas_regional-effectifs-d-etudiants-inscrits.csv"))
#' destfile|>
#' telecharger_fresratlasregionaleffectifsdetudiantsinscrits()|>
#' lire_fresratlasregionaleffectifsdetudiantsinscrits()
lire_fresratlasregionaleffectifsdetudiantsinscrits<-function(destfile){
  destfile|>
    read.csv(sep = ";")|>
    dplyr::filter(Année.universitaire=="2020-21",
                  Identifiant.du.département=="D078")}



#' @description
#' Telecharge le fichier csv depuis
#' https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-atlas_regional-effectifs-d-etudiants-inscrits/export/?disjunctive.rgp_formations_ou_etablissements&sort=-rentree#' Telecharge
#'@param fichier destination
#' @examples 
#' ListeEt78<-lire_ListeEt78()
#' save(ListeEt78,file=
#' file.path(Mydirectories::gitlab.directory(),"Lectures/2022-2023 _ UVSQ Enquêtes Quantitatives.R/Paquet R/data/ListeEt78.rda"))


lire_ListeEt78<-function(){
  system.file("extdata/Liste établissements ESR Yvelines.xlsx",
            package="LEnquetesQuantitativesUVSQ2022")|>
    readxl::read_excel(sheet = 1)|>
    (`[`)(1:114,)}
















#' @description
#' Telecharge le fichier zip depuis
#' "https://www.insee.fr/fr/information/2510634"
#' lien zip:
#' https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-atlas_regional-effectifs-d-etudiants-inscrits/download/?format=csv&disjunctive.rgp_formations_ou_etablissements=true&refine.annee_universitaire=2020-21&refine.niveau_geographique=Commune&refine.dep_id=D078&timezone=Europe/Berlin&lang=en&use_labels_for_header=true&csv_separator=%3B
#' @param fichier destination
#' @examples 
#' destfile=
#' file.path(Mydirectories::gitlab.directory(),"Lectures/2022-2023 _ UVSQ Enquêtes Quantitatives.R/Paquet R/inst/extdata/epci.csv")
#' destfile|>
#' telecharger_fresratlas_regionaleffectifsdetudiantsinscritsCOM() 
telecharger_epci<-
  function(){
    options(timeout = 300)
    zipfile=file.path(tempdir(),"Intercommunalite_Metropole_au_01-01-2022.zip")
    utils::download.file("https://www.insee.fr/fr/statistiques/fichier/2510634/Intercommunalite_Metropole_au_01-01-2022.zip",
                         destfile=zipfile)
    zipfile|>utils::unzip(exdir = tempdir())}

#' @description
#' lit le fichier csv fr-esr-statistiques-sur-les-effectifs-d-etudiants-inscrits-par-etablissement.csv
#'@param fichier destination
#' @examples 
#' lire_epci()[[2]]->epci
#' save(epci,file=
#' file.path(Mydirectories::gitlab.directory(),"Lectures/2022-2023 _ UVSQ Enquêtes Quantitatives.R/Paquet R/data/epci.rda"))
lire_epci<-function(destfile=telecharger_epci()){
  destfile|>
    (function(x){plyr::alply(1:4,1,function(i){readxl::read_xlsx(x,sheet=i,skip = 5)})})()->xx}




#"https://www.insee.fr/fr/information/2510634"
